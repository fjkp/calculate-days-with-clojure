(ns calculate-days.core
  (:require [clj-time.core :as t])
  (:gen-class))

(defn calculate-days
  "Calculate total number of days till today from the given date"
  [d m y]
  (t/in-days (t/interval (t/date-time y m d) (t/now))))

(defn -main
  [& args]
  (println "Total number of days till today is" (calculate-days 4 9 1981)))
