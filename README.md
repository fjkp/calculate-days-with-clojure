# calculate-days

Calculate total number of days till today from the given date

## Installation

- git clone

## Usage

Prerequisite
- [Leiningen](https://leiningen.org/)

```
$ lein run
```
